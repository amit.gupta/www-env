package{ "nginx":
	ensure=> installed
}
service { "nginx":
	ensure=> running
}
file { "/etc/nginx/sites-enabled/test":
	ensure=> present,
	content=>'
		location /{
			root /www
		}
		#location /proxy/ {
		#	proxy "http:url/proxy/"
		#}
		'
}
	
